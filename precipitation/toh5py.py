import h5py
import pandas as pd
import os
import numpy as np
import datetime
from tqdm.auto import tqdm

SAVE_PATH = '/ws_data/CWB/precipitation/raw'
if not os.path.exists(SAVE_PATH):
    os.makedirs(SAVE_PATH)    

df = pd.read_pickle("/ws_data/CWB/precipitation/data/Merge_45x45.pkl")
df_h = pd.read_pickle("/ws_data/CWB/precipitation/data/Height20m_45x45.pkl")
df_loc = pd.read_pickle("/ws_data/CWB/precipitation/data/LATLON_45x45_9place.pkl")
raw_col_names = [c for c in df.columns if 'rain' in c]
cities_names = [city[5:] for city in raw_col_names]
print(raw_col_names)
print(cities_names)
print(df.shape, df_h.shape, df_loc.shape)

df = df.reset_index()
df_h = df_h.reset_index()
df_loc = df_loc.reset_index()

f = h5py.File(os.path.join(SAVE_PATH, 'raw.hdf5'), 'w')

for col in df_h.columns:
    if 'height' in col:
        att = col[:len('height')]
        city = col[len('height')+1:]
        f.create_dataset(f'{city}/{att}', data=np.array(df_h.loc[0, col], dtype=np.float32))
for col in df_loc.columns:
    if 'lon' in col or 'lat' in col:
        att = col[:len('lon')]
        city = col[len('lon')+1:]
        f.create_dataset(f'{city}/{att}', data=np.array(df_loc.loc[0, col], dtype=np.float32))
        
for i in tqdm(range(df.shape[0])):
    time_str = datetime.datetime.strftime(df.loc[i, 'DateTime'], '%Y/%m/%d/%H-%M-%S')
    for c in raw_col_names:
        city = c[5:]
        f.create_dataset(f'{city}/rain/{time_str}', data=np.array(df.loc[i, c], dtype=np.float32))
        
f.close()