import os
import os.path
import tensorflow as tf
import keras
import time
import pandas as pd
import numpy as np
from random import randint
from keras.optimizers import SGD, Adam, Adadelta
from keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger
from keras import backend as K
from keras.models import load_model
import math
from keras.layers import Input, Dense, ConvLSTM2D, Flatten, Dropout, BatchNormalization, Activation, LeakyReLU, GaussianNoise
from keras.models import Sequential
from keras.models import Model
from sklearn.model_selection import train_test_split
from keras.utils import multi_gpu_model
import gc
from sklearn import preprocessing
from sklearn.utils import shuffle

#cuda.select_device(0,1)

os.environ['TF_CPP_MIN_LOG_LEVEL'] = 'PCI_BUS_ID'
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1,2,3,4,5,6,7"

def mse(y_true, y_pred):
    return K.mean(K.square(y_pred - y_true), axis=-1)

def rmse(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))

def step_decay(epoch):
    initial_lrate = 0.1
    drop = 0.5
    epochs_drop = 10.0
    lrate = initial_lrate * math.pow(drop, math.floor((1+epoch)/epochs_drop))
    return lrate

def batch_generator_try(X, y, batch_size, shuffle):
        number_of_batches = np.ceil(X.shape[0]/batch_size)
        counter = 0
        sample_index = np.arange(X.shape[0])
        if shuffle:
            np.random.shuffle(sample_index)
        while True:
            batch_index = sample_index[batch_size*counter:batch_size*(counter+1)]
            X_batch = X[batch_index,:]
            y_batch = y[batch_index]
            counter += 1
            yield X_batch, y_batch
            if (counter == number_of_batches):
                if shuffle:
                    np.random.shuffle(sample_index)
                counter = 0
                
def ConvLSTM(input_shape=None, output_shape=None, period=1, predict_period=6, filter=32, kernel_size=[5, 5]):

    target_f = predict_period*output_shape[0]*output_shape[1]

    inputs = Input(shape=(period, input_shape[0], input_shape[1], 1), name='Input')

    convlstm1 = ConvLSTM2D(filters=filter, kernel_size=kernel_size, padding='same', name='ConvLSTM_l1', activation='tanh', return_sequences=True)(inputs)

    convlstm2 = ConvLSTM2D(filters=filter, kernel_size=kernel_size, padding='same', name='ConvLSTM_l2', activation='tanh', return_sequences=False)(convlstm1)
    
    flat = Flatten()(convlstm2)
    
    dense1 = Dense(64, name = 'dense1')(flat)
    
    leaky1 = LeakyReLU(alpha=0.1)(dense1)
    
    dense2 = Dense(32, name = 'dense2')(leaky1)
    
    leaky2 = LeakyReLU(alpha=0.1)(dense2)
    
    output = Dense(target_f, activation='linear')(leaky2)

    model = Model(inputs=inputs, outputs=output)

    return model
    
gpu_options = tf.GPUOptions(allow_growth=True)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
tf.keras.backend.set_session(sess)
#K.set_session(sess)
if __name__ == "__main__":

    period = 7
    pred_period = 6
    data_height = 55  
    data_width = 55
    origin_height = 35
    origin_width = 35
    origin_height2 = 35
    origin_width2 = 35
    target_f = 1
    pred_pers = 6
    epoch = 600
    train_batch = 32
    test_batch = 32
    Training = True
    Model_name = "2019051720_2layer_3year_55_places"

    save_root = "/home/xb139/Model_Saved/" + Model_name
    if not os.path.isdir(save_root):
        os.mkdir(save_root)

    df2 = pd.read_pickle("/home/xb139/20172019_places_55_no2019051720.pkl")
    df = df2.set_index('DateTime_Five')
    print(df.dtypes)
    bias_station = ['bias_Five', 'bias_Chigu', 'bias_Kandin', 'bias_Chinchangun', 'bias_Hualien', 'bias_Ali_mountain']
    print(df)
    print('loading......')
    
    X = []
    y = []
    for j in range(6):
        for i in range(df.shape[0]):
            X_period_bias = []
            y_period = []
            timeseries = pd.date_range(df.index[i], periods=period+pred_period, freq='10T')
            try:
                for t in timeseries[:6]:
                    X_bias = np.array(df[bias_station[j]].loc[str(t)])[int((data_height-origin_height)/2):int((data_height-origin_height)/2)+origin_height, 
                                int((data_width-origin_width)/2):int((data_width-origin_width)/2)+origin_width]
                    X_period_bias.append(X_bias)
                for t in timeseries[-1:]:
                    y_radar = np.array(df[bias_station[j]].loc[str(t)])[int((data_height-origin_height2)/2):int((data_height-origin_height2)/2)+origin_height2, 
                                int((data_width-origin_width2)/2):int((data_width-origin_width2)/2)+origin_width2]
                
                    y_period.append(y_radar)
    
                X.append(X_period_bias)
                y.append(y_period)
    
            except:
                continue

    X = np.array(X, dtype='float32')
    y = np.array(y, dtype='float32')
    y = y.reshape(y.shape[0], 35*35)
    X = np.nan_to_num(X)
    y = np.nan_to_num(y)
    print(X.shape)
    print(y.shape)        


    for period in range(6, 7):
        for pred_per in range(6, 7):
            save_branch = save_root + "/{}x{}to10m_T{}toT{}".format(origin_height, origin_width, period, pred_per)
            if not os.path.isdir(save_branch):
                os.mkdir(save_branch)
            learning_rate = 0.01
            decay_rate = learning_rate / epoch
            Adadelta = Adadelta(lr=learning_rate, decay= decay_rate)
            
            if Training:
               #K.clear_session()
                print("="*30)
                print("Training T{} pred T{} Strat!".format(period, pred_per))
                print("="*30)
                # ---Loading Data---
                indices = np.arange(X.shape[0])
                np.random.shuffle(indices)
                X = X[indices]
                y = y[indices]
                train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.33, random_state=50)
                print(train_X.shape)
                train_X = train_X.reshape(-1, period, origin_height, origin_width, 1)
                test_X = test_X.reshape(-1, period, origin_height, origin_width, 1)
                tStart = time.time()

                training_log = CSVLogger(save_branch+"/Training.log",append=True)
                earlystop = EarlyStopping(monitor='val_rmse', patience=20, verbose=1, mode='auto')
                checkpoint = ModelCheckpoint(save_branch+"/checkpoint.{epoch:02d}-train(mse)_{loss:.5f}-train(rmse)_{rmse:.5f}-test(mse)_{val_loss:.5f}-test(rmse)_{val_rmse:.5f}.hdf5", monitor='val_rmse',
                                                verbose=1, save_best_only=True, save_weights_only=True, mode='auto', period=1)
            
                callback = [checkpoint, training_log, earlystop]
                
                model = ConvLSTM(input_shape=[35, 35], output_shape=[35, 35], period=6, predict_period=1, kernel_size=[11, 11], filter=36)
                
                model.summary()
                bmodel = multi_gpu_model(model,8)
                bmodel.compile(optimizer=Adadelta, loss=mse, metrics=[rmse])
                bmodel.fit_generator(generator=batch_generator_try(X=train_X, y=train_y, batch_size=train_batch, shuffle=True),
                                    steps_per_epoch=train_X.shape[0]//train_batch,
                                    epochs=epoch,
                                    verbose=1,
                                    callbacks=callback,
                                    validation_data=batch_generator_try(X=test_X, y=test_y, batch_size=test_batch, shuffle=True),
                                    validation_steps=test_X.shape[0]//test_batch)
                
                train_score = bmodel.evaluate(train_X, train_y, batch_size=train_batch)
                test_score = bmodel.evaluate(test_X, test_y, batch_size=test_batch)
                model.save(save_branch+'/model_ConvLSTM_t{}_t{}.h5'.format(period, pred_per))
                tEnd = time.time()

                with open('/home/xb139/Model_Saved/'+'/2019051720.txt', 'a') as log:
                    log.write("%dx%d to 10m,   T%d to T%d\n" % (origin_height, origin_width, period, pred_per))
                    log.write("Train loss(MSE): %.5f ,Train loss(RMSE):%.5f, Test loss(MSE): %.5f, Test loss(RMSE):%.5f, Time spend: %.3f\n" % (train_score[0], train_score[1], test_score[0], test_score[1], tEnd-tStart))
                print("Train loss(MSE): %.5f ,Train loss(RMSE):%.5f, Test loss(MSE): %.5f, Test loss(RMSE):%.5f" % (train_score[0], train_score[1], test_score[0], test_score[1]))
                print("Training {} T{} pred T{} Finished!".format(Model_name, period, pred_per))
                print("="*30)
          
                del model
                #K.clear_session()
                gc.collect()
