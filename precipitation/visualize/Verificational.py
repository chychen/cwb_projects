import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.colors as colors
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = 'PCI_BUS_ID'
os.environ["CUDA_VISIBLE_DEVICES"] = "3"
class Verification(object):
    def __init__(self, pred, target, threshold=40):
        self.pred = pred
        self.target = target
        self.threshold = threshold
        #self.csi, self.far, self.pod, self.acc = self.confusion_matrix()
        self.csi = self.confusion_matrix()

    def __CSI__(self, a, b, c):
        if a+b+c == 0:
            return None
        return np.round(a/(a+b+c), 5)
        
    #def __FAR__(self, a, b):
        #if a+b == 0:
            #return None
        #return np.round(b/(a+b), 3)

    #def __POD__(self, a, c):
        #if a+c == 0:
            #return None
        #return np.round(a/(a+c), 3)

    #def __ACC__(self, a, b, c, d):
        #if a+b+c+d == 0:
            #return None
        #return np.round((a+d)/(a+b+c+d), 3)

    def confusion_matrix(self):
    
        csi = np.zeros((self.threshold, self.pred.shape[0]), dtype=np.float64)
        #far = np.zeros((self.threshold, self.pred.shape[0]))
        #pod = np.zeros((self.threshold, self.pred.shape[0]))
        #acc = np.zeros((self.threshold, self.pred.shape[0]))
        
        for time in range(self.pred.shape[0]):
            for th in range(0,self.threshold,5):
                a = 0
                b = 0
                c = 0
                d = 0
                for idx in range(self.pred.shape[1]):
                    if self.pred[time][idx] >= th and self.target[time][idx] >= th:
                        a += 1
                    elif self.pred[time][idx] >= th and self.target[time][idx] < th:
                        b += 1
                    elif self.pred[time][idx] < th and self.target[time][idx] >= th:
                        c += 1
                    else:
                        d += 1
                csi[th, time] = self.__CSI__(a, b, c)
                #far[th, time] = self.__FAR__(a, b)
                #pod[th, time] = self.__POD__(a, c)
                #acc[th, time] = self.__ACC__(a, b, c, d)
        #return csi, far, pod, acc
        return csi
