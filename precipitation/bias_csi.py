import os
import tensorflow as tf
import keras
import time
import pandas as pd
import numpy as np
from random import randint
from keras.optimizers import SGD, Adam, Adadelta
from keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger
from data_process.load_try2 import load_data
#from load_try2 import load_data
from keras import backend as K
from keras.models import load_model
import math
from sklearn.model_selection import KFold
from keras.layers import Input, LSTM, Dense, Bidirectional, ConvLSTM2D, Flatten, Activation, Dropout
from keras.models import Sequential
from keras.models import Model
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib
from collections import deque
from sklearn.metrics import mean_squared_error
from keras.utils import multi_gpu_model
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from visualize.Verificational import Verification
import warnings
from scipy.ndimage.filters import gaussian_filter
import time

os.environ['TF_CPP_MIN_LOG_LEVEL'] = 'PCI_BUS_ID'
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"

def mse(y_true, y_pred):
    return K.mean(K.square(y_pred - y_true), axis=-1)

def rmse(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))

def step_decay(epoch):
    initial_lrate = 0.1
    drop = 0.5
    epochs_drop = 10.0
    lrate = initial_lrate * math.pow(drop, math.floor((1+epoch)/epochs_drop))
    return lrate

def reg(x,y):
    coefficients = np.polyfit(x,y,1) 
    p = np.poly1d(coefficients) 
    #coefficient_of_dermination = r2_score(y, p(x))
    return coefficients, p


#gpu_options = tf.GPUOptions(allow_growth=True)
#sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


#tf.keras.backend.set_session(sess)

if __name__ == "__main__":

    tStart = time.time()
    
    bias53 = pd.read_pickle("/home/xb139/2017060104_combinerainbias_385_2la_3year_60_6placesstd.pkl")
    #bias53 = bias53_53.reset_index()
    print(bias53.info())
    bias5353=[]
    for i in range(bias53.shape[0]):
        r3= bias53["revise_bias_whole"][i]
        #r33 = gaussian_filter(r3, sigma=2)
        bias5353.append(r3)
    bias5353 = np.array(bias5353)
    bias5353 = np.maximum(bias5353, 0)
    bias5353 = bias5353.reshape(bias53.shape[0]*385*385,1)
    bias5353 = bias5353.astype('float16')
    print(bias5353.shape)
    
    bias_qp=[]
    for i in range(bias53.shape[0]):
        r3= bias53["qpfqpe_Center"][i]
        bias_qp.append(r3)
    bias_qp = np.array(bias_qp)
    print(bias_qp.shape)
    bias_qp = bias_qp.reshape(bias53.shape[0]*385*385,1)
    bias_qp = bias_qp.astype('float16')
    print(bias_qp.shape)
    
    bias_true=[]
    for i in range(bias53.shape[0]):
        r3= bias53["rain_Center"][i]
        bias_true.append(r3)
    bias_true = np.array(bias_true)
    bias_true = bias_true.reshape(bias53.shape[0]*385*385,1)
    bias_true = bias_true.astype('float16')
    print(bias_true.shape)    

    csi_53=[]
    #csi_532=[]          
    for period in range(1):
        csi_eva = Verification(pred=bias5353[:, period].reshape(-1, 1), target=bias_true[:, period].reshape(-1, 1), threshold=105)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            csi_53.append(np.nanmean(csi_eva.csi, axis=1))
            #csi_532.append(csi_eva.csi)
    csi_53 = np.array(csi_53)
    #csi_532 = np.array(csi_532)
    #np.save('csi_53_tainanradar.npy',csi_532)
    
    csi_qp=[]          
    for period in range(1):
        csi_eva = Verification(pred=bias_qp[:, period].reshape(-1, 1), target=bias_true[:, period].reshape(-1, 1), threshold=105)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            csi_qp.append(np.nanmean(csi_eva.csi, axis=1))
    csi_qp = np.array(csi_qp)

    
    ## Draw thesholds AVG CSI
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 6))
    ax.set_facecolor((229.0/255.0, 229.0/255.0, 229.0/255.0))
    plt.xlim(0, 150)
    plt.ylim(0.0, 1.0)
    plt.xlabel('Thresholds')
    plt.ylabel('CSI')
    plt.title('2018/06/14 ~ 2018/06/20\nThresholds CSI')
    plt.grid(True)
    
    #plt.plot(np.arange(csi_1.shape[1]), [np.nan] + np.mean(csi_1[-1:, 1:], 0).tolist(), 'o--', color='y',label='AVG CSI_bias=1x1')
    plt.plot(np.arange(csi_53.shape[1]), [np.nan] + np.mean(csi_53[-1:, 1:], 0).tolist(), 'o', color='b',label='AVG CSI_bias=385x385')
    plt.plot(np.arange(csi_qp.shape[1]), [np.nan] + np.mean(csi_qp[-1:, 1:], 0).tolist(), 'o', color='m', label='AVG CSI_QPFQPE')  
    plt.legend(loc='upper right')
    
    fig.savefig('std2017060104')

    tEnd = time.time()
    with open('/home/xb139/Trainer/'+'/time_testcsi(cuda).txt', 'a') as log:
        log.write("Time spend: %.3f\n" % (tEnd-tStart))