'''
- drop all zeros, which are useless for training, and the amount is massive. (y_max_zeros_counter: 18183)
'''
import h5py
import pandas as pd
import os
import numpy as np
import datetime
import tensorflow as tf
import time
from tqdm.auto import tqdm

SAVE_PATH = '/ws_data/CWB/precipitation/raw'
TARGET_FILESIZE = 150*1024*1024 # 150MB
NUM_RECORDS = TARGET_FILESIZE/(45*45*4*(6+3+3+1)) # 6:x, 3:month/day/hour, 3:height/lon/lat, 1:y
print(f'NUM_RECORDS: {NUM_RECORDS}')

f = h5py.File(os.path.join(SAVE_PATH, 'raw.hdf5'), 'r') # (default: rdcc_nbytes=1*1024**2, rdcc_nslots=512)
COL_NAMES = list(f.keys())
print(f'COL_NAMES: {COL_NAMES}')

# # 6 == 00-00-00, 00-10-00, 00-20-00, 00-30-00, 00-40-00, 00-50-00 are missing
# print(len(f['rain_Ali_mountain/2017/04/01'].keys()) - 6*24)

# Create a dictionary with features that may be relevant.
def example(x, y):
    def _bytes_feature(value):
        """Returns a bytes_list from a string / byte."""
        if isinstance(value, type(tf.constant(0))):
            # BytesList won't unpack a string from an EagerTensor.
            value = value.numpy()
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    feature = {'y': _bytes_feature(y), 'x': _bytes_feature(x)}
    return tf.train.Example(features=tf.train.Features(feature=feature))


def write_tfrec(all_x, all_y, tfrec_path, is_train):
    # list -> numpy
    all_x = np.array(all_x)
    all_y = np.array(all_y)
    if is_train:
        s = np.arange(all_x.shape[0])
        np.random.shuffle(s) # in-place
        all_x = all_x[s]
        all_y = all_y[s]
    
    with tf.io.TFRecordWriter(tfrec_path) as tfrec_writer:
        for x, y in tqdm(zip(all_x, all_y), total=len(all_x)):
            x_byte = x.tobytes()
            y_byte = y.tobytes()
            tf_example = example(x_byte, y_byte)
            tfrec_writer.write(tf_example.SerializeToString())
            
# no overlap!!
def preprocessing(f, start_date, end_date, is_train, PERIOD=6, PREDICT_PERIOD=6):
    # PERIOD = 6 # 1hour
    # PREDICT_PERIOD = 6 # 1hour later
    all_x = []
    all_y = []

    sub_folder = 'train' if is_train else 'test'
    tfrec_folder_path = os.path.join(SAVE_PATH, sub_folder)
    if not os.path.exists(tfrec_folder_path):
        os.makedirs(tfrec_folder_path)
        
    tfrec_path = os.path.join(tfrec_folder_path, 'shard_{}.tfrec')
    tfrec_counter = 0
    
    y_max_zeros_counter = 0
    
    delta_date = end_date - start_date
    for rain_station in tqdm(COL_NAMES):
        height = f[f'{rain_station}/height'][:] # (45,45), dtype=float32
        lon = f[f'{rain_station}/lon'][:] # (45,45), dtype=float32
        lat = f[f'{rain_station}/lat'][:] # (45,45), dtype=float32
        for i in tqdm(range(0, int(delta_date.total_seconds()/600), PERIOD)): # every 10 minutes, and no overlap!!
            x = []
            y = None
            # y
            date_y = start_date + (i+PERIOD+PREDICT_PERIOD) * datetime.timedelta(minutes=10)
            date_str = datetime.datetime.strftime(date_y, '%Y/%m/%d/%H-%M-%S')
            try:
                y = f[f'{rain_station}/rain/{date_str}'][:] # (45,45), dtype=float32
            except KeyError:
#                 print(f'{rain_station}/rain/{date_str} KeyError')
                continue
            if y.max()==0 and is_train: 
                y_max_zeros_counter = y_max_zeros_counter + 1
                continue
            # x
            x_start_date = start_date + i * datetime.timedelta(minutes=10)
            x_break = False
            for ii in range(PERIOD):
                date = x_start_date + ii * datetime.timedelta(minutes=10)
                date_str = datetime.datetime.strftime(date, '%Y/%m/%d/%H-%M-%S')
                try:
                    tmp_x = f[f'{rain_station}/rain/{date_str}'][:] # (45,45), dtype=float32
                except KeyError:
#                     print(f'{rain_station}/rain/{date_str} KeyError')
                    x_break = True
                    break
                x.append(tmp_x)
            if x_break:
                continue
            month = np.ones_like(tmp_x, dtype=np.float32)*date.month
            day = np.ones_like(tmp_x, dtype=np.float32)*date.day
            hour = np.ones_like(tmp_x, dtype=np.float32)*date.hour
            x = x + [month, day, hour, height, lon, lat]
            np_x = np.stack(x, axis=-1)
            
            all_x.append(np_x)
            all_y.append(y)
            if len(all_x) > NUM_RECORDS:
                write_tfrec(all_x, all_y, tfrec_path.format(tfrec_counter), is_train)
                tfrec_counter = tfrec_counter + 1
                all_x = []
                all_y = []
        print(f'rain_station: {rain_station}, y_max_zeros_counter: {y_max_zeros_counter}')
    # last tfrec
    write_tfrec(all_x, all_y, tfrec_path.format(tfrec_counter), is_train)

if __name__ == '__main__':
    # Training
    start_date = datetime.datetime.strptime('2017/04/01/01-10-00', '%Y/%m/%d/%H-%M-%S')
    end_date = datetime.datetime.strptime('2018/10/01/00-00-00', '%Y/%m/%d/%H-%M-%S')
    preprocessing(f, start_date, end_date, is_train=True)
    
    # Testing
    start_date = datetime.datetime.strptime('2019/04/01/01-10-00', '%Y/%m/%d/%H-%M-%S')
    end_date = datetime.datetime.strptime('2019/10/01/00-00-00', '%Y/%m/%d/%H-%M-%S')
    preprocessing(f, start_date, end_date, is_train=False)