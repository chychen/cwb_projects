import numpy as np
import os
import datetime
from glob import glob
from tqdm.auto import tqdm


def tally_norm(data_path):
    tally = {'mean_x': [], 'std_x': [], 'mean_y': [], 'std_y': []}
    tally2 = {
        'min_x': np.inf,
        'max_x': -np.inf,
        'min_y': np.inf,
        'max_y': -np.inf
    }
    for x_path in tqdm(glob(os.path.join(data_path, '*_x.npy')), leave=False):
        y_path = x_path[:-5] + 'y.npy'
        mask_path = x_path[:-5] + 'mask.npy'
        mask = np.load(mask_path)  # (12, 433, 388, 14)
        mask_x = np.concatenate([np.zeros(mask.shape[:3] + (2, )), mask],
                                axis=-1)  # (12, 433, 388, 16)
        mask_y = np.concatenate([mask, mask], axis=-1)  # (12, 433, 388, 28)

        x = np.load(x_path)  # (12, 433, 388, 16)
        x = np.ma.masked_array(x, mask_x)
        mean_x = np.mean(x, axis=(0, 1, 2))  # (16,)
        std_x = np.std(x, axis=(0, 1, 2))  # (16,)
        tally['mean_x'].append(mean_x.data)
        tally['std_x'].append(std_x.data)
        tally2['min_x'] = x.data.min(
        ) if x.data.min() < tally2['min_x'] else tally2['min_x']
        tally2['max_x'] = x.data.max(
        ) if x.data.max() > tally2['max_x'] else tally2['max_x']

        y = np.load(y_path)  # (12, 433, 388, 28)
        y = np.ma.masked_array(y, mask_y)
        mean_y = np.mean(y, axis=(0, 1, 2))  # (28,)
        std_y = np.std(y, axis=(0, 1, 2))  # (28,)
        tally['mean_y'].append(mean_y.data)
        tally['std_y'].append(std_y.data)
        tally2['min_y'] = y.data.min(
        ) if y.data.min() < tally2['min_y'] else tally2['min_y']
        tally2['max_y'] = y.data.max(
        ) if y.data.max() > tally2['max_y'] else tally2['max_y']

    for key in tally:
        tally[key] = np.stack(tally[key], axis=0)
        tally[key] = np.mean(tally[key], axis=(0, )).astype(np.float32)
        print(f'key:{key}, shape:{tally[key].shape}, dtype:{tally[key].dtype}')
        np.save(os.path.join(ROOT_PATH, f'{key}.npy'), tally[key])

    print(tally2)


if __name__ == '__main__':
    ROOT_PATH = '/ws_data/CWB/doppler_wind/'
    TRAIN_RAW_PATH = os.path.join(f'{ROOT_PATH}', f'train_new_big_pic')
    print(ROOT_PATH)
    tally_norm(TRAIN_RAW_PATH)
