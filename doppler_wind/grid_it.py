# import time
# time.sleep(60*60*3)
import tensorflow as tf
import numpy as np
import os
import datetime
from glob import glob
from tqdm.auto import tqdm

GRID_SIZE = 32
NUM_SHARDS_PER_DATE = 10

# Create a dictionary with features that may be relevant.
def example(x, y):
    def _bytes_feature(value):
        """Returns a bytes_list from a string / byte."""
        if isinstance(value, type(tf.constant(0))):
            # BytesList won't unpack a string from an EagerTensor.
            value = value.numpy()
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    feature = {'y': _bytes_feature(y), 'x': _bytes_feature(x)}
    return tf.train.Example(features=tf.train.Features(feature=feature))


def grit_it(data_path, save_path, OVERLAP_RATIO):
    for x_path in tqdm(glob(os.path.join(data_path, '*_x.npy')), leave=False):
        date = x_path[-12:-6]
        y_path = x_path[:-5] + 'y.npy'
        mask_path = x_path[:-5] + 'mask.npy'
        old_x = np.load(x_path)  # (12, 433, 388, 16)
        y = np.load(y_path)  # (12, 433, 388, 28)
        mask = np.load(mask_path)  # (12, 433, 388, 14)
        x = np.concatenate([old_x, mask], axis=-1)  # (12, 433, 388, 16+14)
        _, width, height, _ = x.shape
        x_all_grids = []
        y_all_grids = []
        stride = int(GRID_SIZE * (1. - OVERLAP_RATIO))
        for w in range(0, width - GRID_SIZE, stride):
            for h in range(0, height - GRID_SIZE, stride):
                x_all_grids.append(x[:, w:w + GRID_SIZE, h:h + GRID_SIZE])
                y_all_grids.append(y[:, w:w + GRID_SIZE, h:h + GRID_SIZE])
            x_all_grids.append(x[:, w:w + GRID_SIZE,
                                 height - GRID_SIZE:height])
            y_all_grids.append(y[:, w:w + GRID_SIZE,
                                 height - GRID_SIZE:height])
        for h in range(0, height - GRID_SIZE, stride):
            x_all_grids.append(x[:, width - GRID_SIZE:width, h:h + GRID_SIZE])
            y_all_grids.append(y[:, width - GRID_SIZE:width, h:h + GRID_SIZE])
        x_all_grids = np.concatenate(x_all_grids, axis=0)
        y_all_grids = np.concatenate(y_all_grids, axis=0)
        #         print(x_all_grids.shape, x_all_grids.dtype, y_all_grids.shape, y_all_grids.dtype)

        shard_len = len(x_all_grids) // NUM_SHARDS_PER_DATE
        x_shards = np.split(
            x_all_grids,
            [shard_len * i for i in range(1, NUM_SHARDS_PER_DATE)])
        y_shards = np.split(
            y_all_grids,
            [shard_len * i for i in range(1, NUM_SHARDS_PER_DATE)])
        for idx, (x_shard, y_shard) in enumerate(zip(x_shards, y_shards)):
            with tf.io.TFRecordWriter(
                    os.path.join(save_path,
                                 f'{date}_{idx}.tfrec')) as tfrec_writer:
                for x_data, y_data in tqdm(zip(x_shard, y_shard),
                                           total=len(x_shard)):
                    x_byte = x_data.tobytes()
                    y_byte = y_data.tobytes()
                    tf_example = example(x_byte, y_byte)
                    tfrec_writer.write(tf_example.SerializeToString())


if __name__ == '__main__':
    ROOT_PATH = '/ws_data/CWB/doppler_wind/'
    TRAIN_RAW_PATH = os.path.join(f'{ROOT_PATH}', f'train_new_big_pic')
    TEST_RAW_PATH = os.path.join(f'{ROOT_PATH}', f'test_new_big_pic')
    TRAIN_SAVE_PATH = os.path.join(ROOT_PATH, f'tfrec_{GRID_SIZE}', f'train')
    TEST_SAVE_PATH = os.path.join(ROOT_PATH, f'tfrec_{GRID_SIZE}', f'test')

    if not os.path.exists(TRAIN_SAVE_PATH):
        os.makedirs(TRAIN_SAVE_PATH)
    if not os.path.exists(TEST_SAVE_PATH):
        os.makedirs(TEST_SAVE_PATH)
    
    print(ROOT_PATH)
    print(TRAIN_SAVE_PATH)
    print(TEST_SAVE_PATH)
    grit_it(TRAIN_RAW_PATH, TRAIN_SAVE_PATH, OVERLAP_RATIO=0.5)
    grit_it(TEST_RAW_PATH, TEST_SAVE_PATH, OVERLAP_RATIO=0.5)
