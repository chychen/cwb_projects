import os
from refactor_by_date import refactor_data
from grid_it import refactor_data
from grid_norm import tally_norm


GRID_SIZE = 32
NUM_SHARDS_PER_DATE = 10
ROOT_PATH = '/ws_data/CWB/doppler_wind/'
TRAIN_RAW_PATH = os.path.join(f'{ROOT_PATH}', f'train_new_big_pic')
TEST_RAW_PATH = os.path.join(f'{ROOT_PATH}', f'test_new_big_pic')
TRAIN_SAVE_PATH = os.path.join(ROOT_PATH, f'tfrec_{GRID_SIZE}', f'train')
TEST_SAVE_PATH = os.path.join(ROOT_PATH, f'tfrec_{GRID_SIZE}', f'test')

if not os.path.exists(TRAIN_SAVE_PATH):
    os.makedirs(TRAIN_SAVE_PATH)
if not os.path.exists(TEST_SAVE_PATH):
    os.makedirs(TEST_SAVE_PATH)

# refactor_by_date
refactor_data('train')
refactor_data('test')
# grid_it
grit_it(TRAIN_RAW_PATH, TRAIN_SAVE_PATH, OVERLAP_RATIO=0.5)
grit_it(TEST_RAW_PATH, TEST_SAVE_PATH, OVERLAP_RATIO=0.5)
# get norm
tally_norm(TRAIN_RAW_PATH)
