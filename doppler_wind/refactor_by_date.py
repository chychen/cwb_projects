'''
## Refacctor data by-date instead of by-hour-by-feature

target format:

- x : (12, 433, 388, 16): 
    - 12 = 12 taus
    - 16 = 2 coodinates + 14 levels
- y : (12, 433, 388, 28): 
    - 12 = 12 taus
    - 28 = 14u + 14v
'''

import tensorflow as tf
import numpy as np
import os
import sys
from glob import glob
import datetime
RAW_DATA_PATH = '/ws_data/CWB/doppler_wind/'


def refactor_data(mode):
    if mode == 'train':
        RAW_BIG_PIC_PATH = os.path.join(RAW_DATA_PATH, 'traindata/allPicData')
        NEW_BIG_PIC_PATH = os.path.join(RAW_DATA_PATH, 'train_new_big_pic')
    else:
        RAW_BIG_PIC_PATH = os.path.join(RAW_DATA_PATH, 'testdata')
        NEW_BIG_PIC_PATH = os.path.join(RAW_DATA_PATH, 'test_new_big_pic')

    if not os.path.exists(NEW_BIG_PIC_PATH):
        os.makedirs(NEW_BIG_PIC_PATH)

    xandypos = np.load(os.path.join(RAW_BIG_PIC_PATH, 'xandypos.npy')).astype(
        np.float32)  # (2, 433, 388), float32

    # parse dates from folders' name
    date_str_list = []
    for file_path in glob(os.path.join(RAW_BIG_PIC_PATH, 'rvr_*.npy')):
        date_str_list.append(file_path[-18:-12])
    date_str_list = sorted(date_str_list)
    base = datetime.datetime.strptime(date_str_list[0], "%y%m%d")
    num_days = datetime.datetime.strptime(date_str_list[-1], "%y%m%d") - base
    num_days = num_days.days + 1
    print(f'# days: {num_days}')
    date_list = [base + datetime.timedelta(days=x)
                 for x in range(num_days)]  #20200406 20200501 -
    date_str_list = [date.strftime("%y%m%d") + '12' for date in date_list]

    tostr = lambda var: str(var) if var > 9 else "0" + str(var)
    coor_x = xandypos[0:1]  # (1, 433, 388)
    coor_y = xandypos[1:2]  # (1, 433, 388)
    for date_str in date_str_list:
        out = []
        try:
            invalid_counter = 0
            # input
            all_x = []
            # output
            all_y = []
            # mask
            all_mask = []
            for tau in range(12):
                tau_str = tostr(tau)
                hrs_str = tostr(tau + 12)

                if mode == 'train':
                    rvr = np.load(
                        os.path.join(
                            RAW_BIG_PIC_PATH,
                            f"rvr_{date_str}_{tau_str}_{hrs_str}.npy")
                    )  # (14, 433, 388)
                    u3d = np.load(
                        os.path.join(
                            RAW_BIG_PIC_PATH,
                            f"u3d_{date_str}_{tau_str}_{hrs_str}.npy")
                    )  # (14, 433, 388)
                    v3d = np.load(
                        os.path.join(
                            RAW_BIG_PIC_PATH,
                            f"v3d_{date_str}_{tau_str}_{hrs_str}.npy")
                    )  # (14, 433, 388)
                else:
                    rvr = np.load(
                        os.path.join(
                            RAW_BIG_PIC_PATH,
                            f"rvr_{date_str}_{tau_str}_{hrs_str}.npy")
                    )[::2]  # (14, 433, 388)
                    u3d = np.load(
                        os.path.join(
                            RAW_BIG_PIC_PATH,
                            f"u3d_{date_str}_{tau_str}_{hrs_str}.npy")
                    )[::2]  # (14, 433, 388)
                    v3d = np.load(
                        os.path.join(
                            RAW_BIG_PIC_PATH,
                            f"v3d_{date_str}_{tau_str}_{hrs_str}.npy")
                    )[::2]  # (14, 433, 388)

                # remove invalid
                mask = np.logical_or(u3d == -999., v3d == -999.)
                invalid_counter = invalid_counter + np.count_nonzero(mask)
                rvr[mask] = 0.
                u3d[mask] = 0.
                v3d[mask] = 0.

                tmp_x = np.concatenate([coor_x, coor_y, rvr],
                                       axis=0)  # (16, 433, 388)
                tmp_x = np.transpose(
                    tmp_x,
                    [1, 2, 0])  # (433, 388, 16), channel_last for tensorflow
                all_x.append(tmp_x)

                tmp_y = np.concatenate([u3d, v3d], axis=0)  # (28, 433, 388)
                tmp_y = np.transpose(
                    tmp_y,
                    [1, 2, 0])  # (433, 388, 28), channel_last for tensorflow
                all_y.append(tmp_y)

                mask = np.transpose(
                    mask,
                    [1, 2, 0])  # (433, 388, 14), channel_last for tensorflow
                all_mask.append(mask)

            all_x = np.stack(all_x, axis=0)  # (12, 433, 388, 16)
            all_y = np.stack(all_y, axis=0)  # (12, 433, 388, 28)
            all_mask = np.stack(all_mask, axis=0)  # (12, 433, 388, 14)
            np.save(os.path.join(NEW_BIG_PIC_PATH, f'{date_str[:-2]}_x.npy'),
                    all_x)
            np.save(os.path.join(NEW_BIG_PIC_PATH, f'{date_str[:-2]}_y.npy'),
                    all_y)
            np.save(
                os.path.join(NEW_BIG_PIC_PATH, f'{date_str[:-2]}_mask.npy'),
                all_mask)
            print(
                f'{date_str[:-2]}_x.npy, type={all_x.dtype}, shape={all_x.shape} is done'
            )
            print(
                f'{date_str[:-2]}_y.npy, type={all_y.dtype}, shape={all_y.shape} is done'
            )
            print(
                f'{date_str[:-2]}_mask.npy, type={all_mask.dtype}, shape={all_mask.shape} is done, -999: {invalid_counter}'
            )
        except KeyboardInterrupt:
            sys.exit(0)
        except:
            print(f'{date_str[:-2]}_?.npy is failed!!!!')


if __name__ == '__main__':
    refactor_data('train')
    refactor_data('test')
