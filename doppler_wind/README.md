# Doppler Wind Detection

- note that this is an ongoing project and not a mature solution (might be buggy). please contact author directly if you meet any problems.

## Getting Started

- clone `cwb_projects` and `tf_rlib`

```bash
git clone https://gitlab.com/chychen/cwb_projects
git clone https://gitlab.com/chychen/tf_rlib
```

- setup docker env, `jaycase/tf2` will automatically launch jupyterlab on port 8888 and tensorboard on port 6006
- please make sure `tf_rlib` and datasets `{YOUR_DATA_PATH}` are visible to docker env

```bash
mkdir -p /datasets
docker run --gpus all -id --rm --net host -v $pwd:$pwd -v /datasets:/ws_data/CWB/doppler_wind/ -w $pwd jaycase/tf2
```

- create soft link for datasets, then we should have two folders `train_new_big_pic`, `test_new_big_pic` under the path `/ws_data/CWB/doppler_wind/`

```bash
# mkdir -p /ws_data/CWB/doppler_wind/
ln -s {YOUR_DATA_PATH} /ws_data/CWB/doppler_wind/.
```

## Let's go

- open jupyterlab at local_host:8888 (and surely you could use terminal directly instead of jupyterlab)
- open a terminal~

### Preprocessing

- this step might cost many hours

```bash
cd cwb_projects/doppler_wind
python preprocessing.py
```

### Training

- the following commands will use 4 gpus.

```bash
cd tf_rlib/exp/research/
python dev_doppler_wind.py --loss_fn="mse" --gpus="0,1,2,3" --exp_name="V2RegHResResNet18Runner_mse_lr4e-2_gnorm32_s5w_sgd" --lr=4e-2 --epochs=300 --bs=512 --wrn_radio=1 --conv_norm=GroupNormalization --groups=32
```

### Visualizing Training

- open tensorboard at local_host:6006