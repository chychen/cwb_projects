# CWB SETUP

- 由於運算節點無對外網路，所有課程內容，資料集，預訓練模型都需要手動傳入運算節點。

## 環境設定

1. 在一般電腦上準備好以下資料：
    - 課程內容和建置腳本
        - [AI Bootcamp Repository](https://gitlab.com/chychen/cwb_projects/-/tree/master/gpu_bootcamp)
        - Docker Image: `cwb.tar`
            ```bash
            docker pull jaycase/nvidia:cwb_ai_bootcamp_1
            docker save jaycase/nvidia:cwb_ai_bootcamp_1 > cwb.tar
            ```
    - 預訓練模型
        - [BiT-M R50x1](https://tfhub.dev/google/bit/m-r50x1/1)
    - 資料集
        - [MNIST](https://www.kaggle.com/vikramtiwari/mnist-numpy)
        - [Typhoon Satellite Image](https://drive.google.com/file/d/1yMY1eJ5xe73nTdX_Ie5EO9rkih084fPr/view?usp=sharing)
2. 手動傳入以上資料進運算節點(scp, sftp)，資料夾架構如以下：
    -  gpu_bootcamp
        - (...)
        - cwb.tar
        - start_8_jupyters_v2.sh
        - stop_all_jupyters.sh
        - NVIDIA_AI_Bootcamp.ipynb
        - data
            - BiT-M R50x1 (bit_m-r50x1_1)
            - MNIST (mnist.npz)
            - Typhoon Satellite Image (Image_B13_v2.zip)
3. 載入 docker image

    ```bash
    docker load -i cwb.tar
    ```

4. 在運算節點上執行環境腳本
    - 此腳本會創建8個團隊資料夾(TEAM_0~7)，啟動8個 docker containers。
    - 每個 docker container 會啟動獨立的 Jupyter Lab 在不同的 port 上，使用者可以透過 http:ip_addr:9100 ~ http:ip_addr:9107 連接使用。
    - 每個 port 對應一個docker container環境，分配到一顆 GPU 運算資源和 mount 一個團隊資料夾，每個資料夾會有獨立的課程內容供學員開發測試。

    ```bash
    bash start_8_jupyters_v2.sh
    ```

5. 課程結束，清除所有 docker containers。

    ```bash
    bash stop_all_jupyters.sh
    ```


## (optional) 更新課程

1. 在一般電腦上準備好以下資料：
    - 課程內容：**NVIDIA_AI_Bootcamp.ipynb**
2. 手動傳入以上資料進運算節點(scp, sftp)，資料夾架構如以下：(覆蓋**NVIDIA_AI_Bootcamp.ipynb**)
    -  gpu_bootcamp
        - (...)
        - cwb.tar
        - start_8_jupyters_v2.sh
        - stop_all_jupyters.sh
        - **NVIDIA_AI_Bootcamp.ipynb** 
        - data
            - BiT-M R50x1 (bit_m-r50x1_1)
            - MNIST (mnist.npz)
            - Typhoon Satellite Image (Image_B13_v2.zip)
3. 在運算節點上執行環境腳本 (同環境設定步驟4)
4. 課程結束，清除所有 docker containers。 (同環境設定步驟5)
