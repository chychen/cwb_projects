# CWB x NVIDIA Weather AI Bootcamp #1

## Get Started

### Prerequisites

- Hardware Configuration
    - HPE Apollo 6500
    - CPU：8 x 4 cores
    - GPU: 8 x V100 32GB
    - RAM：8 x 32 GB
    - SSD/HDD：8 x 1GB 
- Software Configuration
    - NVIDIA GPU Driver (>418.x) [(Tensorflow v2.3 Requirement)](https://www.tensorflow.org/install/gpu#software_requirements)
    - [docker](https://docs.docker.com/engine/install/) (>19.03)
    - [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) (>2.1)

### Setup Env by Docker image

- Docker image is copied from NVIDIA [NGC Tensorflow](https://ngc.nvidia.com/catalog/containers/nvidia:tensorflow), which is optimzied for NVIDIA GPU.
```bash
docker pull jaycase/nvidia:cwb_ai_bootcamp_1
```

- Download the environment setup scrips

```bash
git clone https://gitlab.com/chychen/cwb_projects.git
cd cwb_projects/gpu_bootcamp
```

- Create 8 jupyter labs.
    - http://{ip-address}:{port}, ports are valid from 9100~9107, one port for one team, for example http://x.x.x.x:9100, http://x.x.x.x:9107.

```bash
bash start_8_jupyters_v2.sh
```

- Interact with hands-on Lab on Jupyter Lab.

- After hands-on tutorial, we could stop all jupyter labs by following script.

```bash
bash stop_all_jupyters.sh
```

## Acknowledgement

- please contact Jay Chen (jaych@nvidia.com) if any questions.
