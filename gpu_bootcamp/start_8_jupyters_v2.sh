#!/bin/sh
NGPU=8
for PORT in $(seq 0 $((NGPU-1)))
do
    if test -d "TEAM_$PORT"; then
        echo "please backup TEAM folders, and delete them all. (sudo rm -rf TEAM_*)"
        exit 0
    else
        mkdir -m 777 TEAM_$PORT
        cp NVIDIA_AI_Bootcamp.ipynb TEAM_$PORT/.
        chmod 777 TEAM_$PORT/NVIDIA_AI_Bootcamp.ipynb
    fi
    docker run --net host --gpus device=$PORT --name AI_BOOTCAMP_$PORT --shm-size=1g --ulimit memlock=-1 -id -v $(pwd)/data:/workspace/AI_BOOTCAMP_LAB/data -v $(pwd)/TEAM_$PORT:/workspace/AI_BOOTCAMP_LAB --rm jaycase/nvidia:cwb_ai_bootcamp_1 bash -c "jupyter lab --NotebookApp.token="" --allow-root --ip=0.0.0.0 --port=910$PORT"
done
