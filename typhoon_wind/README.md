# Typhoon Wind Highest Wind Classificaiton

- A training script for typhoon highest wind classificaiton application.
    - demo how to use [BiT](https://github.com/google-research/big_transfer)(Big Transfer) transfer learning on satellite image for classificaion applicaion.

## Getting Started

### Setup Data

- create two folder `$ROOT_PATH/train`, `$ROOT_PATH/test`
- put all data under `$ROOT_PATH/train`
- run following script to move every 20 images from `train` to `test` folder
```bash
cd $ROOT_PATH
```
```bash
for VARIABLE in `ls train`; 
do 
echo $VARIABLE; 
mv `ls train/$VARIABLE/* | tail -20` test/$VARIABLE/.
done
```

### Setup Code

```bash
cd $ROOT_PATH
git clone https://gitlab.com/chychen/cwb_projects.git
```

### Setup Env by Docker image

- make sure you have install [nvidia-docker](https://github.com/NVIDIA/nvidia-docker)
- then, start containner by following commands

```bash
docker run -it --rm --gpus all --net host -v $ROOT_PATH:$ROOT_PATH -w $ROOT_PATH tensorflow/tensorflow:latest-gpu-py3 bash
pip install jupyterlab tensorflow_hub matplotlib
jupyter lab --NotebookApp.token="" --allow-root --ip=0.0.0.0 &
```

- JupyterLab: open up your browser `http://{ip address}:8888` or `http://127.0.0.1:8888`

```bash
tensorboard --logdir=/results &
```

- Tensorboard:open up your browser `http://{ip address}:6006` or `http://127.0.0.1:6006`

## Training

- open up the jupyter notebook `$ROOT_PATH/cwb_projects/typhoon_wind/baseline-mae.ipynb`
- please modify `root_dir` variable to your data path correctly before training
- start training

## Acknowledgement

- please contact Jay Chen (jaych@nvidia.com) if any questions.