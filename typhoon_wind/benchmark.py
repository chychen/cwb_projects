import tensorflow as tf
import tensorflow_hub as hub
import matplotlib.pyplot as plt
import numpy as np
import os
import math
from glob import glob
from tensorflow.python.keras.metrics import MeanMetricWrapper
from tensorflow.keras.mixed_precision import experimental as mixed_precision

root_dir = '/ws_data/CWB/typhoon/Image_B13/'
os.environ["CUDA_VISIBLE_DEVICES"]="0"
mixed_float16 = True
if mixed_float16:
    policy = mixed_precision.Policy('mixed_float16')
    mixed_precision.set_policy(policy)
strategy = tf.distribute.MirroredStrategy()
print('Number of devices: {}'.format(strategy.num_replicas_in_sync))

lr = 1e-5 * strategy.num_replicas_in_sync
epochs = 150
BATCH_SIZE_PER_REPLICA = 64
if mixed_float16:
    lr = lr*2.
    epochs = epochs/2.
    BATCH_SIZE_PER_REPLICA = BATCH_SIZE_PER_REPLICA*2
BATCH_SIZE = BATCH_SIZE_PER_REPLICA * strategy.num_replicas_in_sync
epochs = math.ceil(epochs/strategy.num_replicas_in_sync)

class CustomMAE(MeanMetricWrapper):
    def __init__(self,
                 name='CustomMAE',
                 dtype=None):
        super(CustomMAE, self).__init__(self.mae,
                                                name,
                                                dtype=dtype)

    def mae(self, y_true, y_pred):
        y_true = tf.math.argmax(y_true, axis=-1)
        y_pred = tf.math.argmax(y_pred, axis=-1)
        ret = tf.math.abs(y_true-y_pred)
        return ret

train_ds = tf.data.Dataset.list_files(os.path.join(root_dir, 'train/*/*'), shuffle=True)
test_ds = tf.data.Dataset.list_files(os.path.join(root_dir, 'test/*/*'), shuffle=True)

class_names = np.array(sorted([os.path.basename(item) for item in glob(os.path.join(root_dir,'train/*'))]))
print(class_names)
num_class = len(class_names)

with strategy.scope():
    model = tf.keras.applications.ResNet50V2(
        include_top=True, weights=None, classes=num_class, classifier_activation='softmax'
    )

    optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
    loss_fn = tf.keras.losses.CategoricalCrossentropy(from_logits=False, label_smoothing=0.0)

    model.compile(optimizer=optimizer,
                  loss=loss_fn,
                  metrics=['accuracy', tf.keras.metrics.AUC(num_thresholds=200), tf.keras.metrics.TopKCategoricalAccuracy(k=3, name='top3'), tf.keras.metrics.TopKCategoricalAccuracy(k=5, name='top5'), CustomMAE()])

@tf.function
def process_path(file_path):
    parts = tf.strings.split(file_path, os.path.sep)
    label = parts[-2] == class_names
    img = tf.io.read_file(file_path)
    img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.resize(img, [224, 224])
    # Norm
    img = img / 128.0 -1
    return img, label

train_ds = train_ds.map(process_path, num_parallel_calls=tf.data.experimental.AUTOTUNE).cache()
test_ds = test_ds.map(process_path, num_parallel_calls=tf.data.experimental.AUTOTUNE).cache()


model.build([None, 224,224,3])
# model.summary()

num_train = tf.data.experimental.cardinality(train_ds).numpy()
print(f'num_train: {num_train}')
num_test = tf.data.experimental.cardinality(test_ds).numpy()
print(f'num_test: {num_test}')

STEPS_PER_EPOCH = math.ceil(num_train/BATCH_SIZE) * 10

@tf.function
def aug(img, y):
    pad=28
    img = tf.image.resize_with_crop_or_pad(img, 224 + pad * 2, 224 + pad * 2)
    img = tf.image.random_crop(img, [224, 224, 3])
    img = tf.image.random_flip_left_right(img)
    return img, y

train_ds = train_ds.map(aug, num_parallel_calls=tf.data.experimental.AUTOTUNE).shuffle(
        num_train).batch(BATCH_SIZE, drop_remainder=True).repeat().prefetch(
            buffer_size=tf.data.experimental.AUTOTUNE)
test_ds = test_ds.cache().batch(
    BATCH_SIZE, drop_remainder=False).prefetch(
        buffer_size=tf.data.experimental.AUTOTUNE)

# Fine-tune model
history = model.fit(
    train_ds,
    batch_size=BATCH_SIZE,
    steps_per_epoch=STEPS_PER_EPOCH,
    epochs=epochs,  
    validation_data=test_ds
)